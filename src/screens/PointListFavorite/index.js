import React from 'react';
import {
    StyleSheet,
    ScrollView
} from 'react-native';
import {CompanyListFavoriteComponent} from "../../components/index";
import CONST from "../../styles/CONST";

export default class PointListFavoriteScreen extends React.Component {
    state = {
        company: [
            {
                name: 'Alfa bank 1',
                image: {uri: 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/256/sign-check-icon.png'},
                points: [
                    {
                        pointName: 'Главный офис',
                        pointDistance: '110 m',
                        pointFrom: 'улица Кольская, д. 7, корп. 25'
                    },
                    {
                        pointName: 'Главный офис',
                        pointDistance: '110 m',
                        pointFrom: 'улица Кольская, д. 7, корп. 25'
                    },
                    {
                        pointName: 'Главный офис',
                        pointDistance: '110 m',
                        pointFrom: 'улица Кольская, д. 7, корп. 25'
                    }
                ]
            },
            {
                name: 'Alfa bank 2',
                image: {uri: 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/256/sign-check-icon.png'},
                points: [
                    {
                        pointName: 'Главный офис',
                        pointDistance: '110 m',
                        pointFrom: 'улица Кольская, д. 7, корп. 25'
                    },
                    {
                        pointName: 'Главный офис',
                        pointDistance: '110 m',
                        pointFrom: 'улица Кольская, д. 7, корп. 25'
                    },
                    {
                        pointName: 'Главный офис',
                        pointDistance: '110 m',
                        pointFrom: 'улица Кольская, д. 7, корп. 25'
                    }
                ]
            },
            {
                name: 'Alfa bank 2',
                image: {uri: 'http://photoshopworld.ru/lessons/645/final.jpg'},
                points: [
                    {
                        pointName: 'Главный офис',
                        pointDistance: '110 m',
                        pointFrom: 'улица Кольская, д. 7, корп. 25'
                    },
                    {
                        pointName: 'Главный офис',
                        pointDistance: '110 m',
                        pointFrom: 'улица Кольская, д. 7, корп. 25'
                    },
                    {
                        pointName: 'Главный офис',
                        pointDistance: '110 m',
                        pointFrom: 'улица Кольская, д. 7, корп. 25'
                    }
                ]
            }
        ]

    };


    _genCompanyList = () => {
        return (
            this.state.company.map((item, key) => (
                <CompanyListFavoriteComponent
                    key={key}
                    itemImage={item.image}
                    itemName={item.name}
                    onPress={() => {
                        console.log(item.image)
                    }}
                    pointsArray={item.points}

                />
            ))
        );
    };

    render() {
        return (
            <ScrollView contentContainerStyle={styles.scroll}>
                {this._genCompanyList()}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        ...CONST.container
    },
    scrollColor: {
        backgroundColor: CONST.colors.white
    }
});