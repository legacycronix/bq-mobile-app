import React from 'react';
import {
    StyleSheet,
    ScrollView
} from 'react-native';
import CompanyListComponent from "../../components/CompanyList/index";
import CONST from "../../styles/CONST";

export default class PointListScreen extends React.Component {
    state = {
        company: [
            {
                name: 'Alfa bank 1',
                image: {uri: 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/256/sign-check-icon.png'}
            },
            {
                name: 'Alfa bank 2',
                image: {uri: 'http://icons.iconarchive.com/icons/paomedia/small-n-flat/256/sign-check-icon.png'}
            },
            {
                name: 'Alfa bank 2',
                image: {uri: 'http://photoshopworld.ru/lessons/645/final.jpg'}
            }
        ]
    };


    _genCompanyList = () => {
        return (
            this.state.company.map((item, key) => (
                <CompanyListComponent
                    key={key}
                    itemImage={item.image}
                    itemName={item.name}
                    onPress={() => {
                        this.props.navigation.navigate()
                    }}
                />
            ))
        );
    };

    render() {
        return (
            <ScrollView contentContainerStyle={styles.scroll}>
                {this._genCompanyList()}
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        ...CONST.container
    },
    scrollColor: {
        backgroundColor: CONST.colors.white
    }
});