import CompanyListScreen from "./CompanyList";
import CompanyListFavoriteScreen from "./CompanyListFavorite";

export {
    CompanyListScreen,
    CompanyListFavoriteScreen
}