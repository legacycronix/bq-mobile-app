import React from 'react';
import {TabNavigator} from 'react-navigation';
import {Image} from 'react-native';
import {height, width} from "react-native-dimension";
import {
    CompanyListScreen,
    CompanyListFavoriteScreen
} from "../screens/index";

const routeConfig = {
    CompanyList: {
        screen: CompanyListScreen,
        navigationOptions: {
            title: 'Список'
        }
    },
    CompanyListFavorite: {
        screen: CompanyListFavoriteScreen,
        navigationOptions: {
            title: 'Избранное'
        }
    },
};

const tabOptions = {
    animationEnabled: true,
    tabBarPosition: 'top',
    swipeEnabled: true,
    tabBarOptions: {}
};


const CompanyTabNavigator = TabNavigator(routeConfig, {
    initialRouteName: 'CompanyList',
    ...tabOptions,
});


export default CompanyTabNavigator