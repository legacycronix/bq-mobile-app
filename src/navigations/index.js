import React from 'react';
import {Platform} from 'react-native';
import {StackNavigator, DrawerNavigator, DrawerItems, TabNavigator} from 'react-navigation';
import CompanyTabNavigator from "./CompanyTabNavigator";


const Stack = {

    CompanyListRoute: {
        screen: CompanyTabNavigator,
        navigationOptions: {
            title:'Организации'
        }
    },
    PointListRoute: {
        screen: CompanyTabNavigator,
        navigationOptions: {
            title:'Организации'
        }
    },

};


export default RootNavigator = (initRoute) => {
    return (
        StackNavigator({
            ...Stack
        }, {
            transitionConfig: () => ({
                containerStyle: {}
            }),
            initialRouteName: initRoute,
            headerMode: 'screen',
            headerBackButtonText: Platform.OS === 'ios' ? 'Back' : 0
        }))
}
