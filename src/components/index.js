import CompanyListComponent from "./CompanyList";
import CompanyListFavoriteComponent from "./CompanyListFavorite";

export {
    CompanyListComponent,
    CompanyListFavoriteComponent
}