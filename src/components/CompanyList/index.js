import React from 'react';
import CONST from "../../styles/CONST";
import PropTypes from 'prop-types';
import {Thumbnail} from 'native-base';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';
import {
    height,
    width
} from "react-native-dimension";


export default class CompanyListComponent extends React.Component {
    state = {};
    static propTypes = {
        onPress: PropTypes.func,
        itemName: PropTypes.string,
        itemImage: PropTypes.object,

    };

    static defaultProps = {
        onPress: () => null,
        itemName: null,
        itemImage: null,

    };

    render() {
        const {onPress, itemName, itemImage} = this.props;
        return (
            <TouchableOpacity style={styles.companyList} onPress={onPress}>
                <View style={styles.companyView}>
                    <Text>
                        {itemName}
                    </Text>
                    <Thumbnail source={itemImage}/>
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        ...CONST.container
    },
    companyList: {
        ...CONST.heightCompanyList
    },
    companyView: {
        ...CONST.container,
        ...CONST.rowCenter,
        paddingHorizontal: width(5)
    }
});