import React from 'react';
import CONST from "../../styles/CONST";
import PropTypes from 'prop-types';
import {Thumbnail} from 'native-base';
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
} from 'react-native';
import {
    height,
    width
} from "react-native-dimension";
import Icon from 'react-native-vector-icons/FontAwesome';

export default class CompanyListFavoriteComponent extends React.Component {
    state = {};

    static propTypes = {
        onPress: PropTypes.func,
        itemName: PropTypes.string,
        itemImage: PropTypes.object,

        pointsArray: PropTypes.array,

    };

    static defaultProps = {
        onPress: () => null,
        itemName: null,
        itemImage: null,

        pointsArray: [
            {
                pointName: null,
                pointDistance: null,
                pointFrom: null
            }
        ],
    };

    _genCompanyPoints = () => {
        const {pointsArray} = this.props;

        return (
            pointsArray.map((item, key) => (
                <TouchableOpacity key={key} style={styles.pointList}
                                  onPress={() => {
                                      console.log(key)
                                  }}>
                    <View style={styles.pointIcon}>
                        <Icon name={'map-marker'} size={height(2.5)} color={CONST.colors.gray}/>
                    </View>
                    <View style={styles.pointContent}>
                        <View style={styles.topContent}>
                            <Text>
                                {item.pointName}
                            </Text>
                            <Text>
                                {item.pointDistance}
                            </Text>
                        </View>
                        <View style={{flex: 1, justifyContent: 'center'}}>
                            <Text>
                                {item.pointFrom}
                            </Text>
                        </View>
                    </View>
                </TouchableOpacity>
            ))
        )
    };

    render() {
        const {onPress, itemName, itemImage} = this.props;
        return (
            <View>
                <TouchableOpacity style={styles.companyList} onPress={onPress}>
                    <View style={styles.companyView}>
                        <Text>
                            {itemName}
                        </Text>
                        <Thumbnail source={itemImage}/>
                    </View>
                </TouchableOpacity>
                {this._genCompanyPoints()}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        ...CONST.container
    },
    companyList: {
        ...CONST.heightCompanyList
    },
    companyView: {
        ...CONST.container,
        ...CONST.rowCenter,
        paddingHorizontal: width(5)
    },
    pointList: {
        flexDirection: 'row',
        ...CONST.heightCompanyPoint
    },
    pointIcon: {
        ...CONST.container,
        ...CONST.center,
    },
    pointContent: {
        flex: 5
    },
    topContent: {
        ...CONST.container,
        ...CONST.rowCenter,
        paddingRight: width(5)
    }
});