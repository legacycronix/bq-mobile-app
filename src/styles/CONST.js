import {
    height,
    width
} from "react-native-dimension";

export default {
    colors: {
        white: '#FFFFFF',
        black: '#000000',
        gray: '#DADADA',
        placeholder: '#BBBBBB',
        green: '#6BC048'
    },
    container: {
        flex: 1
    },
    rowCenter:{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    textFamily: {
        fontFamily: 'Roboto'
    },
    center: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    heightCompanyList:{
        flex:1,
        height:height(12.5)
    },
    heightCompanyPoint:{
        flex:1,
        height:height(10)
    }
}