import React from 'react';
import {AppLoading} from 'expo';
import {StatusBar, I18nManager} from "react-native";
import RootNavigator from './src/navigations/index'

export default class App extends React.Component {
    state = {
        fontLoaded: false,
    };

    componentDidMount() {
        StatusBar.setHidden(true);
        I18nManager.allowRTL(false);
        I18nManager.forceRTL(false);
    }

    async componentWillMount() {
        await Expo.Font.loadAsync({
            'Arial': require('native-base/Fonts/Roboto.ttf'),
            'Roboto': require('native-base/Fonts/Roboto.ttf'),
            'Roboto_medium': require('native-base/Fonts/Roboto_medium.ttf'),

        });
        this.setState({fontLoaded: true});
    }

    render() {
        const {fontLoaded} = this.state;

        const initialRoute = 'CompanyListRoute';
        const AppNavigator = RootNavigator(initialRoute);

        if (!fontLoaded) {
            return (<AppLoading/>)
        }

        return <AppNavigator/>

    }
}